package br.com.felix.adminserver

import de.codecentric.boot.admin.server.config.EnableAdminServer
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient

@EnableAdminServer
@EnableEurekaClient
@SpringBootApplication
class AdminServerApplication

fun main(args: Array<String>) {
	runApplication<AdminServerApplication>(*args)
}
