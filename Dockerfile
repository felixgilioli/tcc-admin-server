FROM openjdk:11-jdk-slim
ENV APP_HOME=/usr/app/
WORKDIR $APP_HOME
COPY ./build/libs/* ./admin-server-0.0.1-SNAPSHOT.jar
EXPOSE 8086
CMD ["java","-jar","admin-server-0.0.1-SNAPSHOT.jar"]